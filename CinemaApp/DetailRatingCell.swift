//
//  DetailRatingCell.swift
//  CinemaApp
//
//  Created by Miklos Pavel on 2017. 04. 04..
//  Copyright © 2017. Miklos Pavel. All rights reserved.
//

import UIKit

class DetailRatingCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var ratingSlider: UISlider!
    @IBAction func sliderMoved(_ sender: UISlider) {
        sender.setValue(round(ratingSlider.value), animated: true)
        changeTrackColor(rating: round(ratingSlider.value))
    }    

    override func awakeFromNib() {
        super.awakeFromNib()
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(sliderTapped(gestureRecognizer:)))
        ratingSlider.addGestureRecognizer(tapGestureRecognizer)
        changeTrackColor(rating: 5)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func sliderTapped(gestureRecognizer: UIGestureRecognizer) {
        let pointTapped = gestureRecognizer.location(in: self)
        
        let positionOfSlider = ratingSlider.frame.origin
        let widthOfSlider = ratingSlider.frame.size.width
        let newValue = ((pointTapped.x - positionOfSlider.x) * CGFloat(ratingSlider.maximumValue) / widthOfSlider)
        let newRating = Float(round(newValue))
        
        ratingSlider.setValue(newRating, animated: true)
        changeTrackColor(rating: newRating)
    }
    
    private func changeTrackColor(rating: Float) {
        switch rating {
        case 0...2:
            ratingSlider.minimumTrackTintColor = sliderColor.red
            ratingSlider.thumbTintColor = sliderColor.red
        case 2...6:
            ratingSlider.minimumTrackTintColor = sliderColor.yellow
            ratingSlider.thumbTintColor = sliderColor.yellow
        case 6...10:
            ratingSlider.minimumTrackTintColor = sliderColor.green
            ratingSlider.thumbTintColor = sliderColor.green
        default:
            break
        }
    }
    
    struct sliderColor {
        static let red = UIColor(rgb: 0xec434f)
        static let yellow = UIColor(rgb: 0xffca30)
        static let green = UIColor(rgb: 0x68d080)
    }
}

