//
//  MoviesViewController.swift
//  CinemaApp
//
//  Created by Miklos Pavel on 2017. 04. 04..
//  Copyright © 2017. Miklos Pavel. All rights reserved.
//

import UIKit

class MoviesViewController: UIViewController {
    
    //MARK: - Outlets
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    
    let cvDataSource = CollectionViewDataSource()    
    var movies: [Movie]?
    var tapToDismissKeyboard: UITapGestureRecognizer?
    lazy var movieManager = MovieManager()
    
    //MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = cvDataSource
        searchBar.delegate = self
        setupSearchBar()
        
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            self?.movies = self?.movieManager.fetchMovies()
            self?.cvDataSource.movies = self?.movies
            
            DispatchQueue.main.async { [weak self] in
                self?.collectionView.reloadData()
            }
        }
    }
    
    //MARK: - Methods
    
    private func setupSearchBar() {
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSFontAttributeName:UIFont(name: "Open Sans", size: 14)!]
        tapToDismissKeyboard = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        view.addGestureRecognizer(tapToDismissKeyboard!)
        tapToDismissKeyboard?.isEnabled = false
    }
    
    func handleTap() {
        searchBar.resignFirstResponder()
        tapToDismissKeyboard?.isEnabled = false
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let targetVC = segue.destination as? DetailTVC {
            if let cell = sender as? MovieCell {
                targetVC.movie = cell.movie
                targetVC.image = cell.imageView.image
            }
        }
    }
}

extension MoviesViewController: UISearchBarDelegate {
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        tapToDismissKeyboard?.isEnabled = true
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        tapToDismissKeyboard?.isEnabled = false
        searchBar.resignFirstResponder()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty {
            cvDataSource.movies = movies
            collectionView.reloadData()
            return
        }
        let query = searchText.lowercased()
        cvDataSource.movies = movies?.filter { movie in
            return movie.title?.lowercased().contains(query) ?? false
        }
        collectionView.reloadData()
    }
}
