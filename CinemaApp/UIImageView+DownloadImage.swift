//
//  UIImageView+DownloadImage.swift
//  CinemaApp
//
//  Created by Miklos Pavel on 2017. 04. 04..
//  Copyright © 2017. Miklos Pavel. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func loadImageWithURL(url: URL) -> URLSessionDownloadTask {
        let session = URLSession.shared
        let downloadTask = session.downloadTask(with: url) { [weak self] localUrl, response, error in
            if error == nil,
                let localUrl = localUrl,
                let data = try? Data(contentsOf: localUrl),
                let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    if let strongSelf = self {
                        strongSelf.image = image
                    }
                }
            }
        }
        downloadTask.resume()
        return downloadTask
    }
}
