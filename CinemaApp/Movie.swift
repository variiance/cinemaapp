//
//  Movie.swift
//  CinemaApp
//
//  Created by Miklos Pavel on 2017. 04. 04..
//  Copyright © 2017. Miklos Pavel. All rights reserved.
//

import Foundation


struct Movie {
    let title: String?
    let year: Int?
    let plot: String?
    let poster: URL?
    
    init(fromJSON json: [String: AnyObject]) {
        self.title = json["title"] as? String
        if let year = json["year"] as? String {
            self.year = Int(year)
        } else {
            self.year = nil
        }
        self.plot = json["plot"] as? String
        if let poster = json["poster"] as? String {
            self.poster = URL(string: poster)
        } else {
            self.poster = nil
        }
    }
}
