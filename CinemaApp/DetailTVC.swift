//
//  DetailTVC.swift
//  CinemaApp
//
//  Created by Miklos Pavel on 2017. 04. 05..
//  Copyright © 2017. Miklos Pavel. All rights reserved.
//

import UIKit
import MXParallaxHeader

class DetailTVC: UITableViewController {
    
    var movie: Movie!
    var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = movie.title
        setTableViewAppearance()
        setupParallaxHeader()
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movie.plot != nil ? 2 : 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.ratingCell) as! DetailRatingCell
            if let title = movie.title {
                cell.nameLabel.text = title
                if let year = movie.year {
                    cell.yearLabel.text = "(\(year))"
                }
            }
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: Identifiers.plotCell) as! DetailPlotCell
            cell.textView.text = movie.plot
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    //MARK: - Table view delegate
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func setTableViewAppearance() {
        tableView.estimatedRowHeight = 150
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
    }
    
    func setupParallaxHeader() {
        let headerView = UIImageView()
        headerView.image = image
        headerView.contentMode = .scaleAspectFill
        tableView.parallaxHeader.view = headerView
        tableView.parallaxHeader.height = image.size.height
        tableView.parallaxHeader.mode = MXParallaxHeaderMode.topFill
        tableView.parallaxHeader.minimumHeight = 20
    }
    
    struct Identifiers {
        static let imageCell = "ImageCell"
        static let ratingCell = "RatingCell"
        static let plotCell = "PlotCell"
    }
}


