//
//  CVDataSource.swift
//  CinemaApp
//
//  Created by Miklos Pavel on 2017. 04. 04..
//  Copyright © 2017. Miklos Pavel. All rights reserved.
//

import UIKit

class CollectionViewDataSource: NSObject, UICollectionViewDataSource {
    
    var movies: [Movie]?
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifiers.movieCell,
                                                      for: indexPath
            ) as! MovieCell
        
        if let movies = movies {
            cell.movie = movies[indexPath.row]
        }
        
        return cell
    }
    
    struct Identifiers {
        static let movieCell = "MovieCell"
    }
}
