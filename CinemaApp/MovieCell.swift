//
//  MovieCell.swift
//  CinemaApp
//
//  Created by Miklos Pavel on 2017. 04. 04..
//  Copyright © 2017. Miklos Pavel. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    private var downloadTask: URLSessionDownloadTask?
    var movie: Movie? {
        didSet {
            if let poster = movie?.poster {
                updateImage(url: poster)
            }
            if let title = movie?.title {
                nameLabel.text = title
                if let year = movie?.year {
                    yearLabel.text = "(\(year))"
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.image = UIImage(named: "placeholder.jpg")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        downloadTask?.cancel()
        downloadTask = nil
        nameLabel.text = ""
        yearLabel.text = ""
        imageView.image = UIImage(named: "placeholder.jpg")
    }
    
    func updateImage(url: URL) {
        downloadTask = imageView.loadImageWithURL(url: url)
    }
}
