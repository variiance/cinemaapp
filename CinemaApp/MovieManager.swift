//
//  MovieManager.swift
//  CinemaApp
//
//  Created by Miklos Pavel on 2017. 04. 04..
//  Copyright © 2017. Miklos Pavel. All rights reserved.
//

import Foundation

class MovieManager {
    
    func fetchMovies() -> [Movie]? {
        guard let json = createJSON() else { return nil }
        
        var movies: [Movie] = []
        for movie in json {
            let mov = Movie(fromJSON: movie)
            movies.append(mov)
        }
        
        return movies
    }
    
    private func performURLRequest() -> Data? {
        let urlString = "http://ponte.hu/mobil/ios/movies.json"
        let url = URL(string: urlString)
        do {
            return try Data(contentsOf: url!)
        } catch {
            print("Error: \(error)")
            return nil
        }
    }
    
    private func createJSON() -> [[String: AnyObject]]? {
        guard let data = performURLRequest() else { return nil }
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: AnyObject]]
        } catch {
            print("Error: \(error)")
            return nil
        }
    }
}
